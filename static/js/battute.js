var jsonCopione = "";
var nomePersona = "GIUSE";


function updateBattuteFromIndex(index){
    var prevIndex = index;
    var index = trovaBattutaSuccessiva(index);

    $(".battuta1").empty();
    $(".battuta2").empty();
    for (i = prevIndex; i < index; i++){
        if (i >= 0)
            $(".battuta1").append(getHtmlBattuta(i));
    }
    $(".battuta2").append(getHtmlBattuta(index));
    $(".battuta2").find(".text").hide();

    return index;
}

function downloadCopione(nomeCopione, callback){
    $.getJSON( "copioni/" + nomeCopione, function( data ) {
        jsonCopione = data['copione'];
        callback();
    });
}

function trovaBattutaSuccessiva(index){
    for (i = index + 1; i < jsonCopione.length; i++){
        if (jsonCopione[i]['name'] == nomePersona)
            return i;
    }
    return -1;
}

function getHtmlBattuta (index){
    var wrapper = $("<h3/>");
    var elem = $("<span/>");
    var name = $("<strong class='name'/>");
    var text = $("<span class='text'/>");

    name.text(jsonCopione[index]['name'] + ": ");
    text.text(jsonCopione[index]['text']);

    wrapper.append (elem);
    elem.append (name);
    elem.append (text);

    return wrapper;
}