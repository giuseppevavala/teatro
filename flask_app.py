import os
import traceback
from werkzeug import secure_filename

from flask import render_template
from flask import Flask
from flask import request

import utility

app = Flask(__name__)

UPLOAD_FOLDER = '/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



@app.route('/rest/ping', methods=['GET'])
def ping():
    return "{status: 'ok'}", 200


# API per uplodare un copione, trasformarlo in JSON e salvarlo in modo persistente
@app.route('/rest/copione', methods=['POST'])
def upload_file():
    try:
        file = request.files['file']
        jsonCopione = utility.jsonfyCopione (file)
        filename = secure_filename(file.filename)
        filePath = "./teatro/static/copioni/" + os.path.splitext(os.path.basename(filename))[0].lower()

        persistentFile = open (filePath, "w")
        persistentFile.write (jsonCopione)
        persistentFile.close ()
        return "{status: 'ok', filePath: '" + filePath +"'}", 200
    except:
        print(traceback.format_exc())
        return "{status: 'error'}", 200



"""
cop = Copione()

@app.route('/', methods=['GET'])
def index ():
	return render_template('index.html', persone = cop.getPersone().keys())

@app.route('/count', methods=['GET'])
def count ():
    ris = {}
    for p in cop.getPersone().keys():
        if (cop.getCountBattuteStudiate(p)> 0): ris[p] = cop.getCountBattuteStudiate(p)
    return json.dumps (ris)


@app.route('/<name>', methods=['GET', 'POST'])
def copione(name = None ):
	if request.method == 'POST':
		pos = int(request.form['pos'])
		print name, pos
		index = cop.next(pos, name)
		cop.countBattutaStudiata(index, name)
		return json.dumps (cop.getForTemplate(index))
	else:
		index = cop.next(-1, name)
		cop.countBattutaStudiata(index, name)
		return render_template('battute.html', data=cop.getForTemplate(index), index=index, total=cop.getCountBattuteTotali(name), name=name)

@app.route('/<name>/battutaMenoStudiata', methods=['POST'])
def getBattutaMenoStudiata(name = None ):
	index = cop.getBattutaMenoStudiata(name)
	cop.countBattutaStudiata(index, name)
	return json.dumps (cop.getForTemplate(index))

@app.route('/copione', methods=['POST'])
def uploadNewCopione(name = None ):
	index = cop.getBattutaMenoStudiata(name)
	cop.countBattutaStudiata(index, name)
	return json.dumps (cop.getForTemplate(index))

"""