import re
import json

def jsonfyCopione (file):
	lines = file.readlines()
	index = 0
	result = []
	for line in lines:
		if (line != '\n'):
			isNew = re.match (r'^(\w+|\w+\s?\w+)(\s?):(.*)', line)
			if (isNew):
				el = {}
				el['name'] = isNew.group(1).upper().lstrip()
				el['text'] = isNew.group(3).lstrip()
				el['index'] = index
				index = index + 1
				result.append (el)
			else:
				if ( index < len(result) ):
				    result[index]['battuta'] = result[index]['battuta'] + el
	jsonResult = {}
	jsonResult['copione'] = result
	return json.dumps (jsonResult)